package soa.mvp.parkingfinder.service;

import android.hardware.SensorEventListener;

/**
 * Created by raulvillca on 20/5/17.
 */

public interface SensorCallback {
    void onClick();
}
