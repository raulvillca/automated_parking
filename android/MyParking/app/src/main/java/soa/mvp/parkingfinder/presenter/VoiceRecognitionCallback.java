package soa.mvp.parkingfinder.presenter;

/**
 * Created by raulvillca on 18/5/17.
 */

public interface VoiceRecognitionCallback {
    void notifyRecognizer();
}
